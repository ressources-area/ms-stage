package org.ressourcesarea.MSStage.integrationTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSStage.model.dtos.NoteStageDto;
import org.ressourcesarea.MSStage.repository.StageRepository;
import org.ressourcesarea.MSStage.service.contract.NoteStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class NoteStageServiceIntegrationTest {

    @Autowired
    private NoteStageService noteStageService;

    @Autowired
    private StageRepository stageRepository;

    @BeforeEach
    public void setUp(){

    }

    @Test
    void createNoteStage(){
        Integer idStage = 1 ;

        NoteStageDto noteStageDto = new NoteStageDto();
        noteStageDto.setNote("Note de test");
        noteStageDto.setTitre("Titre de la note de test");

        noteStageService.createNoteStage(noteStageDto,idStage);

        assertThat(noteStageService.getListNoteStageByIdStage(1)).hasSize(2);
    }

    @Test
    void getListNoteStageByIdStage(){
        assertThat(noteStageService.getListNoteStageByIdStage(1)).hasSize(1);
    }

    @Test
    void deleteNoteStage(){
        noteStageService.deleteNoteStage(1);

        assertThat(noteStageService.getListNoteStageByIdStage(1)).hasSize(0);
    }
}
