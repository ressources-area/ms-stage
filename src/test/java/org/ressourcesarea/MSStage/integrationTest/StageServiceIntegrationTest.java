package org.ressourcesarea.MSStage.integrationTest;

import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSStage.model.dtos.StageDto;
import org.ressourcesarea.MSStage.service.contract.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class StageServiceIntegrationTest {

    @Autowired
    private StageService stageService;

    @Test
    void createStage(){
        StageDto stageDto = new StageDto();
        stageDto.setIntitule("Intitule de stage");
        stageDto.setNumero("Numéro de stage");
        stageDto.setLieu("Lieu du stage");
        stageDto.setDateDebut(LocalDate.now());
        stageDto.setDateFin(stageDto.getDateDebut().plusDays(3));

        stageService.createStage(stageDto);

        assertThat(stageService.getListStage()).hasSize(4);
    }

    @Test
    void getListStageByIntitule(){
        assertThat(stageService.getListStageByIntitule("Equipier SUAP")).hasSize(1);
    }

    @Test
    void getStageById(){
        assertThat(stageService.getStageById(1).getNumero())
                .isEqualTo("SUAP-0001");
    }

    @Test
    void updateStage(){
        StageDto stageDto = stageService.getStageById(1);
        stageDto.setLieu("Angers");

        stageService.updateStage(stageDto);

        assertThat(stageService.getStageById(1).getLieu()).isEqualTo("Angers");
    }

    @Test
    void deleteStage(){
        stageService.deleteStage(1);

        assertThat(stageService.getStageById(1)).isNull();
    }
}
