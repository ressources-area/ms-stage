package org.ressourcesarea.MSStage.integrationTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSStage.model.dtos.AgentStageDto;
import org.ressourcesarea.MSStage.model.entities.AgentStage;
import org.ressourcesarea.MSStage.model.entities.Stage;
import org.ressourcesarea.MSStage.repository.AgentStageRepository;
import org.ressourcesarea.MSStage.repository.StageRepository;
import org.ressourcesarea.MSStage.service.contract.AgentStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class AgentStageServiceIntegrationTest {

    @Autowired
    private AgentStageService agentStageService;

    @Autowired
    private AgentStageRepository agentStageRepository;

    @Autowired
    private StageRepository stageRepository;

    @BeforeEach
    public void setUp(){
        Stage stage = new Stage();
        stage.setIntitule("Test intitule");
        stage.setLieu("Test lieu");
        stage.setNumero("Test numéro");
        stage.setDateDebut(LocalDate.now());
        stage.setDateFin(stage.getDateDebut().plusDays(5));

        stageRepository.save(stage);

        AgentStage agentStage = new AgentStage();
        agentStage.setStage(stage);
        agentStage.setNomAgent("NomTest");
        agentStage.setPrenomAgent("TestPrenom");
        agentStage.setMatriculeAgent("5555");
        agentStage.setStatus("Stagiaire");
        agentStage.setNomCentre("Laval");

        agentStageRepository.save(agentStage);
    }

    @Test
    void createAgent(){
        Integer idStage = 1;

        AgentStageDto agentStageDto = new AgentStageDto();
        agentStageDto.setNomAgent("TotoNom");
        agentStageDto.setPrenomAgent("TotoPrenom");
        agentStageDto.setMatriculeAgent("2222");
        agentStageDto.setStatus("Formateur");
        agentStageDto.setNomCentre("Mayenne");

        agentStageService.createAgentStage(agentStageDto, idStage);
        assertThat(agentStageRepository.getAgentStageByIdStage(idStage)).hasSize(2);
    }

    @Test
    void getAgentStageByIdStage(){
        assertThat(agentStageService.getAgentStageByIdStage(1)).hasSize(1);
    }

    @Test
    void getAgentStageById(){
        assertThat(agentStageService.getAgentStageById(1)).isNotNull();
    }

    @Test
    void getAgentStageByMatriculeAgent(){
        String matricule = "4821";
        assertThat(agentStageService.getAgentStageByMatriculeAgent(matricule)).hasSize(3);
    }

    @Test
    void getMatriculeInStage(){
        assertThat(agentStageService.getMatriculeInStage(1)).hasSize(1);
    }

    @Test
    void updateAgentStage(){
        AgentStageDto agentStageDto = agentStageService.getAgentStageById(1);

        agentStageDto.setMatriculeAgent("8888");
        agentStageService.updateAgentStage(agentStageDto);

        assertThat(agentStageRepository.getAgentStageById(1).getMatriculeAgent())
                .isEqualTo("8888");
    }

    @Test
    void deleteAgentStage(){
        agentStageService.deleteAgentStage(1);

        assertThat(agentStageService.getAgentStageById(1)).isNull();
    }

}

