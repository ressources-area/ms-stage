package org.ressourcesarea.MSStage.service.impl;

import org.ressourcesarea.MSStage.model.dtos.NoteStageDto;
import org.ressourcesarea.MSStage.model.dtos.StageDto;
import org.ressourcesarea.MSStage.model.mapper.NoteStageMapper;
import org.ressourcesarea.MSStage.repository.NoteStageRepository;
import org.ressourcesarea.MSStage.service.contract.NoteStageService;
import org.ressourcesarea.MSStage.service.contract.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NoteStageServiceImpl implements NoteStageService {

    // ----- Injections des dépendances ----- //

    private final NoteStageRepository noteStageRepository;
    private final NoteStageMapper noteStageMapper;
    private final StageService stageService;

    @Autowired
    public NoteStageServiceImpl(NoteStageRepository noteStageRepository,
                                NoteStageMapper noteStageMapper,
                                StageService stageService){
        this.noteStageRepository = noteStageRepository;
        this.noteStageMapper = noteStageMapper;
        this.stageService = stageService;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param noteStageDto
     * @return
     */
    @Override
    @Transactional
    public NoteStageDto createNoteStage(final NoteStageDto noteStageDto, Integer idStage) {
        StageDto stageDto = stageService.getStageById(idStage);
        noteStageDto.setStage(stageDto);

        return noteStageMapper.fromEntityToDto(
                noteStageRepository.save(noteStageMapper.fromDtoToEntity(noteStageDto))
        );
    }

    /**
     * {@inheritDoc}
     * @param idStage
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<NoteStageDto> getListNoteStageByIdStage(final Integer idStage) {
        return noteStageMapper.mapToDto(noteStageRepository.getListNoteStageByIdStage(idStage));
    }

    /**
     * {@inheritDoc}
     * @param idNoteStage
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public NoteStageDto getNoteStageById(final Integer idNoteStage) {
        return noteStageMapper.fromEntityToDto(noteStageRepository.getNoteStageById(idNoteStage));
    }

    /**
     * {@inheritDoc}
     * @param noteStageDto
     * @return
     */
    @Override
    @Transactional
    public NoteStageDto updateNoteStage(final NoteStageDto noteStageDto) {
        return noteStageMapper.fromEntityToDto(
                noteStageRepository.save(noteStageMapper.fromDtoToEntity(noteStageDto))
        );
    }

    /**
     * {@inheritDoc}
     * @param idNoteStage
     */
    @Override
    @Transactional
    public void deleteNoteStage(final Integer idNoteStage) {
        noteStageRepository.delete(noteStageRepository.getNoteStageById(idNoteStage));
    }
}
