package org.ressourcesarea.MSStage.service.impl;

import org.ressourcesarea.MSStage.application.error.EntityException;
import org.ressourcesarea.MSStage.model.dtos.AgentStageDto;
import org.ressourcesarea.MSStage.model.dtos.StageDto;
import org.ressourcesarea.MSStage.model.mapper.StageMapper;
import org.ressourcesarea.MSStage.repository.AgentStageRepository;
import org.ressourcesarea.MSStage.repository.NoteStageRepository;
import org.ressourcesarea.MSStage.repository.StageRepository;
import org.ressourcesarea.MSStage.service.contract.AgentStageService;
import org.ressourcesarea.MSStage.service.contract.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class StageServiceImpl implements StageService {

    // ----- Injections des dépendances ----- //

    private final StageRepository stageRepository;
    private final StageMapper stageMapper;
    private final AgentStageService agentStageService;
    private final NoteStageRepository noteStageRepository;
    private final AgentStageRepository agentStageRepository;

    @Autowired
    public StageServiceImpl(StageRepository stageRepository,
                            StageMapper stageMapper,
                            AgentStageService agentStageService,
                            NoteStageRepository noteStageRepository,
                            AgentStageRepository agentStageRepository){
        this.stageRepository = stageRepository;
        this.stageMapper = stageMapper;
        this.agentStageService = agentStageService;
        this.noteStageRepository = noteStageRepository;
        this.agentStageRepository = agentStageRepository;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param stageDto
     * @return
     */
    @Override
    @Transactional
    public StageDto createStage(final StageDto stageDto) {
        return stageMapper.fromEntityToDto(
                stageRepository.save(stageMapper.fromDtoToEntity(stageDto)));
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<StageDto> getListStage() {
        return stageMapper.mapToDto(stageRepository.getListStage());
    }

    /**
     * {@inheritDoc}
     * @param intitule
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<StageDto> getListStageByIntitule(final String intitule) {
        return stageMapper.mapToDto(stageRepository.getListStageByIntitule(intitule));
    }

    /**
     * {@inheritDoc}
     * @param idStage
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public StageDto getStageById(final Integer idStage) {
        return stageMapper.fromEntityToDto(stageRepository.getStageById(idStage));
    }

    /**
     * {@inheritDoc}
     * @param matricule
     * @return
     */
    @Override
    public List<StageDto> getListStageByMatriculeAgent(final String matricule) {
        try {
            List<StageDto> stageDtoList = new ArrayList<>();

            // On récupère les IDs des stages dans lequels est l'agent via agentStage
            List<AgentStageDto> agentStageDtoList = agentStageService.getAgentStageByMatriculeAgent(matricule);
            // Si aucun agentStage est retourné, la list est null
            if (agentStageDtoList.size()==0){
                return null;
            } else {
                // On boucle sur la liste des agentStage et on add dans la list des stages
                // dont l'agent est affecté.
                for (AgentStageDto agentStage: agentStageDtoList) {
                    StageDto stageDto = new StageDto();
                    stageDto = agentStage.getStage();
                    stageDtoList.add(stageDto);
                }

                return stageDtoList;
            }
        } catch (EntityException e) {
            throw new EntityException("Erreur lors de la récupération des stages");
        }
    }

    /**
     * {@inheritDoc}
     * @param stageDto
     * @return
     */
    @Override
    @Transactional
    public StageDto updateStage(final StageDto stageDto) {
        return stageMapper.fromEntityToDto(
                stageRepository.save(stageMapper.fromDtoToEntity(stageDto))
        );
    }

    /**
     * {@inheritDoc}
     * @param idStage
     */
    @Override
    @Transactional
    public void deleteStage(final Integer idStage) {
        agentStageRepository.deleteAllAgentStageByIdStage(idStage);
        noteStageRepository.deleteAllNoteStageByIdStage(idStage);
        stageRepository.delete(stageRepository.getStageById(idStage));
    }
}
