package org.ressourcesarea.MSStage.service.impl;

import org.ressourcesarea.MSStage.model.dtos.AgentStageDto;
import org.ressourcesarea.MSStage.model.mapper.AgentStageMapper;
import org.ressourcesarea.MSStage.model.mapper.StageMapper;
import org.ressourcesarea.MSStage.repository.AgentStageRepository;
import org.ressourcesarea.MSStage.repository.StageRepository;
import org.ressourcesarea.MSStage.service.contract.AgentStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AgentStageServiceImpl implements AgentStageService {

    // ----- Injections des dépendances ----- //

    private final AgentStageRepository agentStageRepository;
    private final AgentStageMapper agentStageMapper;
    private final StageRepository stageRepository;
    private final StageMapper stageMapper;

    @Autowired
    public AgentStageServiceImpl(AgentStageRepository agentStageRepository,
                                 AgentStageMapper agentStageMapper,
                                 StageRepository stageRepository,
                                 StageMapper stageMapper){
        this.agentStageRepository = agentStageRepository;
        this.agentStageMapper = agentStageMapper;
        this.stageRepository = stageRepository;
        this.stageMapper = stageMapper;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param agentStageDto
     * @param idStage
     * @return
     */
    @Override
    @Transactional
    public AgentStageDto createAgentStage(final AgentStageDto agentStageDto, final Integer idStage) {
        agentStageDto.setStage(stageMapper.fromEntityToDto(stageRepository.getStageById(idStage)));
        return agentStageMapper.fromEntityToDto(
                agentStageRepository.save(agentStageMapper.fromDtoToEntity(agentStageDto))
        );
    }

    /**
     * {@inheritDoc}
     * @param idStage
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentStageDto> getAgentStageByIdStage(final Integer idStage) {
        return agentStageMapper.mapToDto(agentStageRepository.getAgentStageByIdStage(idStage));
    }

    /**
     * {@inheritDoc}
     * @param idAgentStage
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public AgentStageDto getAgentStageById(final Integer idAgentStage) {
        return agentStageMapper.fromEntityToDto(agentStageRepository.getAgentStageById(idAgentStage));
    }

    /**
     * {@inheritDoc}
     * @param matriculeAgent
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentStageDto> getAgentStageByMatriculeAgent(final String matriculeAgent) {
        return agentStageMapper.mapToDto(agentStageRepository.getAgentStageByMatriculeAgent(matriculeAgent));
    }

    /**
     * {@inheritDoc}
     * @param idStage
     * @return
     */
    @Override
    @Transactional
    public List<String> getMatriculeInStage(final Integer idStage) {
        return agentStageRepository.getMatriculeInStage(idStage);
    }

    /**
     * {@inheritDoc}
     * @param agentStageDto
     * @return
     */
    @Override
    @Transactional
    public AgentStageDto updateAgentStage(final AgentStageDto agentStageDto) {
        return agentStageMapper.fromEntityToDto(
                agentStageRepository.save(agentStageMapper.fromDtoToEntity(agentStageDto))
        );
    }

    /**
     * {@inheritDoc}
     * @param idAgentStage
     */
    @Override
    @Transactional
    public void deleteAgentStage(final Integer idAgentStage) {
        agentStageRepository.delete(agentStageRepository.getAgentStageById(idAgentStage));
    }
}
