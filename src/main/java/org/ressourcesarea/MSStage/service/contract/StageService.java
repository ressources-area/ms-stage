package org.ressourcesarea.MSStage.service.contract;

import org.ressourcesarea.MSStage.model.dtos.StageDto;

import java.util.List;

public interface StageService {

    /**
     * Création d'un stage
     * @param stageDto
     * @return
     */
    StageDto createStage(StageDto stageDto);

    /**
     * Récupère la liste de tout les stages
     * @return
     */
    List<StageDto> getListStage();

    /**
     * Récupère la liste des stages par intitulé
     * @param intitule
     * @return
     */
    List<StageDto> getListStageByIntitule(String intitule);

    /**
     * Récupère un stage selon son id
     * @param idStage
     * @return
     */
    StageDto getStageById(Integer idStage);

    /**
     * Récupère les stages dans lesquels est l'agent selon son matricule
     * @param matricule
     * @return
     */
    List<StageDto> getListStageByMatriculeAgent(String matricule);

    /**
     * Modifie un stage
     * @param stageDto
     * @return
     */
    StageDto updateStage(StageDto stageDto);

    /**
     * Supprime un stage
     * @param idStage
     */
    void deleteStage(Integer idStage);
}
