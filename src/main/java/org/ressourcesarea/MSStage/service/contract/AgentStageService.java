package org.ressourcesarea.MSStage.service.contract;

import org.ressourcesarea.MSStage.model.dtos.AgentStageDto;

import java.util.List;

public interface AgentStageService {

    /**
     * Création d'un agentStage
     * @param agentStageDto
     * @param idStage
     * @return
     */
    AgentStageDto createAgentStage(AgentStageDto agentStageDto, final Integer idStage);

    /**
     * récupération de tous les agents dans un stage
     * @param idStage
     * @return
     */
    List<AgentStageDto> getAgentStageByIdStage( Integer idStage);

    /**
     * Récupération de tout les stages d'un agent
     * @param matriculeAgent
     * @return
     */
    List<AgentStageDto> getAgentStageByMatriculeAgent(String matriculeAgent);

    /**
     * Récupération d'un agentStage selon son id
     * @param idAgentStage
     * @return
     */
    AgentStageDto getAgentStageById(Integer idAgentStage);

    /**
     * Récupération de tout les matricules qui ne sont pas dans le stage
     * @param idStage
     * @return
     */
    List<String> getMatriculeInStage(Integer idStage);

    /**
     * Modification d'un agentStage
     * @param agentStageDto
     * @return
     */
    AgentStageDto updateAgentStage( AgentStageDto agentStageDto);

    /**
     * Suppression d'un agentStage
     * @param idAgentStage
     */
    void deleteAgentStage(Integer idAgentStage);
}
