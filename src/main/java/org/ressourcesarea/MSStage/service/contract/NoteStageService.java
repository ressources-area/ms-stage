package org.ressourcesarea.MSStage.service.contract;

import org.ressourcesarea.MSStage.model.dtos.NoteStageDto;

import java.util.List;

public interface NoteStageService {

    /**
     * Création d'un note dans un stage
     * @param noteStageDto
     * @return
     */
    NoteStageDto createNoteStage(NoteStageDto noteStageDto, Integer idStage);

    /**
     * Récupère une liste de note selon un stage
     * @param idStage
     * @return
     */
    List<NoteStageDto> getListNoteStageByIdStage(Integer idStage);

    /**
     * Récupère une note selon son id
     * @param idNoteStage
     * @return
     */
    NoteStageDto getNoteStageById(Integer idNoteStage);

    /**
     * Modifie une note
     * @param noteStageDto
     * @return
     */
    NoteStageDto updateNoteStage(NoteStageDto noteStageDto);

    /**
     * Supprime une note
     * @param idNoteStage
     */
    void deleteNoteStage(Integer idNoteStage);
}
