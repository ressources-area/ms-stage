package org.ressourcesarea.MSStage.model.dtos;

import lombok.Data;

@Data
public class AgentStageDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String status;
    private String matriculeAgent;
    private String nomAgent;
    private String prenomAgent;
    private String nomCentre;
    private StageDto stage;
}
