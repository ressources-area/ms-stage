package org.ressourcesarea.MSStage.model.dtos;

import lombok.Data;
import org.apache.tomcat.jni.Local;

import java.time.LocalDate;
import java.util.Date;

@Data
public class StageDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String intitule;
    private String numero;
    private LocalDate dateDebut;
    private LocalDate dateFin;
    private String lieu;

}
