package org.ressourcesarea.MSStage.model.dtos;

import lombok.Data;

@Data
public class NoteStageDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String titre;
    private String note;
    private StageDto stage;
}
