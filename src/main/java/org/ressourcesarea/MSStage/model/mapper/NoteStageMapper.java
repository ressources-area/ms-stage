package org.ressourcesarea.MSStage.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.ressourcesarea.MSStage.model.dtos.NoteStageDto;
import org.ressourcesarea.MSStage.model.entities.NoteStage;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NoteStageMapper {

    List<NoteStageDto> mapToDto (List<NoteStage> noteStageList);
    List<NoteStage> map (List<NoteStageDto> noteStageDtoList);
    NoteStageDto fromEntityToDto (NoteStage noteStage);
    NoteStage fromDtoToEntity (NoteStageDto noteStageDto);
}
