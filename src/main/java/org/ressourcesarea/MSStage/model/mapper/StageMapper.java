package org.ressourcesarea.MSStage.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.ressourcesarea.MSStage.model.dtos.StageDto;
import org.ressourcesarea.MSStage.model.entities.Stage;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StageMapper {

    List<StageDto> mapToDto (List<Stage> stageList);
    List<Stage> map (List<StageDto> stageDtoList);
    StageDto fromEntityToDto (Stage stage);
    Stage fromDtoToEntity (StageDto stageDto);
}
