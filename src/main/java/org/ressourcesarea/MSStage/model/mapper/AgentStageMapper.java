package org.ressourcesarea.MSStage.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.ressourcesarea.MSStage.model.dtos.AgentStageDto;
import org.ressourcesarea.MSStage.model.entities.AgentStage;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)

public interface AgentStageMapper {

    List<AgentStageDto> mapToDto (List<AgentStage> agentStageList);
    List<AgentStage> map ( List<AgentStageDto> agentStageDtoList);
    AgentStageDto fromEntityToDto (AgentStage agentStage);
    AgentStage fromDtoToEntity (AgentStageDto agentStageDto);
}
