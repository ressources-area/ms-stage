package org.ressourcesarea.MSStage.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "agentstage")
@Data
public class AgentStage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "status")
    private String status;

    @Column(name = "matriculeagent")
    private String matriculeAgent;

    @Column(name = "nomagent")
    private String nomAgent;

    @Column(name = "prenomagent")
    private String prenomAgent;

    @Column(name = "nomcentre")
    private String nomCentre;

    @ManyToOne(fetch = FetchType.LAZY)
    private Stage stage;

}
