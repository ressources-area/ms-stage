package org.ressourcesarea.MSStage.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Data
@Table(name = "stage")
public class Stage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "intitule")
    private String intitule;

    @Column(name = "numero")
    private String numero;

    @Column(name = "datedebut")
    private LocalDate dateDebut;

    @Column(name = "datefin")
    private LocalDate dateFin;

    @Column(name = "lieu")
    private String lieu;

}
