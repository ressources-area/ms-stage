package org.ressourcesarea.MSStage.repository;

import org.ressourcesarea.MSStage.model.entities.Stage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StageRepository extends JpaRepository<Stage, Integer> {

    /* Récupération de tout les stages */
    @Query("SELECT s FROM Stage s")
    List<Stage> getListStage();

    /* Récupération de tout les stages selon l'intitule */
    @Query("SELECT s FROM Stage s WHERE s.intitule = :intitule")
    List<Stage> getListStageByIntitule(@Param("intitule") String intitule);

    /* Récupération d'un stage selon son id */
    @Query("SELECT s FROM Stage s WHERE s.id = :idStage")
    Stage getStageById(@Param("idStage") Integer idStage);
}
