package org.ressourcesarea.MSStage.repository;

import org.ressourcesarea.MSStage.model.entities.NoteStage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteStageRepository extends JpaRepository<NoteStage, Integer> {

    /* Récupère la liste des notes d'un stage */
    @Query("SELECT n FROM NoteStage n WHERE n.stage.id = :idStage")
    List<NoteStage> getListNoteStageByIdStage(@Param("idStage") Integer idStage);

    /* Récupère une note selon son id */
    @Query("SELECT n FROM NoteStage n WHERE n.id = :idStage")
    NoteStage getNoteStageById(@Param("idStage") Integer idStage);

    @Modifying
    @Query("DELETE FROM NoteStage n WHERE n.stage.id = :idStage")
    void deleteAllNoteStageByIdStage(@Param("idStage") Integer idStage);

}
