package org.ressourcesarea.MSStage.repository;

import org.ressourcesarea.MSStage.model.entities.AgentStage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgentStageRepository extends JpaRepository<AgentStage, Integer> {

    /* Récupération de tous les agentStages dans le stage */
    @Query("SELECT a FROM AgentStage a WHERE a.stage.id = :idStage")
    List<AgentStage> getAgentStageByIdStage(@Param("idStage") Integer idStage);

    /* Récupération d'un agentStage selon son id */
    @Query("SELECT a FROM AgentStage a WHERE a.id = :idAgentStage")
    AgentStage getAgentStageById (@Param("idAgentStage") Integer idAgentStage);

    /* Récupération de tout les stages d'un agent par son matricule */
    @Query("SELECT a FROM AgentStage a WHERE a.matriculeAgent = :matriculeAgent")
    List<AgentStage> getAgentStageByMatriculeAgent(@Param("matriculeAgent") String matriculeAgent);

    @Query("SELECT a.matriculeAgent FROM AgentStage a WHERE a.stage.id = :idStage")
    List<String> getMatriculeInStage(@Param("idStage") Integer idStage);

    @Modifying
    @Query("DELETE FROM AgentStage a WHERE a.stage.id = :idStage")
    void deleteAllAgentStageByIdStage(@Param("idStage") Integer idStage);
}
