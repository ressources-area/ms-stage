package org.ressourcesarea.MSStage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("org.ressourcesarea.MSStage")
public class MsStageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsStageApplication.class, args);
		System.out.println("MS-Stage start");
	}

}
