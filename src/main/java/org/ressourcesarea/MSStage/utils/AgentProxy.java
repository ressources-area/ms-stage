package org.ressourcesarea.MSStage.utils;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ms-agent", url = "localhost:8181")
public interface AgentProxy {

    @GetMapping(value = "auth/authSuccess")
    Boolean authSuccess(@RequestParam("token") String token);
}
