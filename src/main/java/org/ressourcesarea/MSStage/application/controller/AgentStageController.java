package org.ressourcesarea.MSStage.application.controller;

import org.ressourcesarea.MSStage.application.error.GeneralException;
import org.ressourcesarea.MSStage.model.dtos.AgentStageDto;
import org.ressourcesarea.MSStage.service.contract.AgentStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("agentStage")
public class AgentStageController {

    // ----- Injections des dépendances ----- //

    private final AgentStageService agentStageService;

    @Autowired
    public AgentStageController(AgentStageService agentStageService){
        this.agentStageService = agentStageService;
    }

    // ----- Méthodes ----- //

    /**
     * Création d'un agentStage
     * @param agentStageDto
     * @return
     */
    @PostMapping("create/{idStage}")
    public AgentStageDto createAgentStage(@RequestBody AgentStageDto agentStageDto,
                                          @PathVariable ("idStage") Integer idStage){
        try {
            agentStageDto = agentStageService.createAgentStage(agentStageDto, idStage);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la création d'un agentStage",e
            );
        }
        return agentStageDto;
    }

    /**
     * Récupération de tous les agents dans un stage
     * @param idStage
     * @return
     */
    @GetMapping("stage/{idStage}")
    public List<AgentStageDto> getAgentStageByIdStage(@PathVariable("idStage") Integer idStage){
        try {
            return agentStageService.getAgentStageByIdStage(idStage);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération de la liste d'agent dans le stage",e
            );
        }
    }

    /**
     * Récupération des stages d'un agent selon son matricule
     * @param matriculeAgent
     * @return
     */
    @GetMapping("/agent/{matriculeAgent}")
    public List<AgentStageDto> getAgentStageByMatriculeAgent(@PathVariable("matriculeAgent") String matriculeAgent){
        try {
            return agentStageService.getAgentStageByMatriculeAgent(matriculeAgent);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération de la liste d'agent selon matricule",e
            );
        }
    }

    /**
     * Récupération d'un agentStage par son id
     * @param idAgentStage
     * @return
     */
    @GetMapping("/{idAgentStage}")
    public AgentStageDto getAgentStageById(@PathVariable("idAgentStage") Integer idAgentStage){
        try {
            return agentStageService.getAgentStageById(idAgentStage);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération de l'agent dans le stage",e
            );
        }
    }

    /**
     * Récupère la list des matricules dans le stage
     * @param idStage
     * @return
     */
    @GetMapping("/getListMatriculeInStage/{idStage}")
    public List<String> getMatriculeListInStage(@PathVariable("idStage") Integer idStage){
        try {
            return agentStageService.getMatriculeInStage(idStage);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération d'une liste de matricule dans le stage",e
            );
        }
    }

    /**
     * Modification d'un agentStage
     * @param agentStageDto
     * @return
     */
    @PutMapping("/update")
    public AgentStageDto updateAgentStage(@RequestBody AgentStageDto agentStageDto){
        try {
            agentStageDto = agentStageService.updateAgentStage(agentStageDto);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification d'un agentStage",e
            );
        }
        return agentStageDto;
    }

    /**
     * Supprime un agentStage
     * @param idAgentStage
     */
    @DeleteMapping("/delete/{idAgentStage}")
    public void deleteAgentStage(@PathVariable("idAgentStage") Integer idAgentStage){
        try {
            agentStageService.deleteAgentStage(idAgentStage);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la suppression d'un agent dans le stage",e
            );
        }
    }
}
