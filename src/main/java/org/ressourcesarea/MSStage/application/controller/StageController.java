package org.ressourcesarea.MSStage.application.controller;

import org.ressourcesarea.MSStage.application.error.GeneralException;
import org.ressourcesarea.MSStage.model.dtos.StageDto;
import org.ressourcesarea.MSStage.service.contract.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("stage")
public class StageController {

    // ----- Injections des dépendances ----- //

    private final StageService stageService;

    @Autowired
    public StageController(StageService stageService){
        this.stageService = stageService;
    }

    // ----- Méthodes ----- //

    /**
     * Création d'un stage
     * @param stageDto
     * @return
     */
    @PostMapping("/create")
    public StageDto createStage(@RequestBody StageDto stageDto){
        try{
            stageDto = stageService.createStage(stageDto);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la création d'un stage", e
            );
        }
        return stageDto;
    }

    /**
     * Récupère tout les stages
     * @return
     */
    @GetMapping("/stageList")
    public List<StageDto> getListStage(){
        return stageService.getListStage();
    }

    /**
     * Récupère la liste des stages selon l'intitulé
     * @param intitule
     * @return
     */
    @GetMapping("/stageList/{intitule}")
    public List<StageDto> getListStageByIntitule(@Param("intitule") String intitule){
        return stageService.getListStageByIntitule(intitule);
    }

    /**
     * Récupère un stage selon son id
     * @param idStage
     * @return
     */
    @GetMapping("/{idStage}")
    public StageDto getStageById(@PathVariable("idStage") Integer idStage){
        return stageService.getStageById(idStage);
    }

    /**
     * Récupère les stages dans lesquels est l'agent selon son matricule
     * @param matricule
     * @return
     */
    @GetMapping("/getListStageByAgent/{matricule}")
    public List<StageDto> getListStageByMatriculeAgent(@PathVariable("matricule") String matricule){
        return stageService.getListStageByMatriculeAgent(matricule);
    }

    /**
     * Modification d'un stage
     * @param stageDto
     * @return
     */
    @PutMapping("/update")
    public StageDto updateStage(@RequestBody StageDto stageDto){
        try {
            stageDto = stageService.updateStage(stageDto);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification du stage",e
            );
        }
        return stageDto;
    }

    /**
     * Suppression d'un stage
     * @param idStage
     */
    @DeleteMapping("delete/{idStage}")
    public void deleteStage(@PathVariable("idStage") Integer idStage){
        try {
            stageService.deleteStage(idStage);
        } catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la suppression du stage",e
            );
        }
    }
}
