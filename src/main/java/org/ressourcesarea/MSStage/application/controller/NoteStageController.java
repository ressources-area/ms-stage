package org.ressourcesarea.MSStage.application.controller;

import org.ressourcesarea.MSStage.application.error.GeneralException;
import org.ressourcesarea.MSStage.model.dtos.NoteStageDto;
import org.ressourcesarea.MSStage.service.contract.NoteStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("noteStage")
public class NoteStageController {

    // ----- Injections des dépendances ----- //

    private final NoteStageService noteStageService;

    @Autowired
    public NoteStageController(NoteStageService noteStageService){
        this.noteStageService = noteStageService;
    }

    // ----- Méthodes ----- //

    /**
     * Création d'une note de stage
     * @param noteStageDto
     * @param idStage
     * @return
     */
    @PostMapping("/create/{idStage}")
    public NoteStageDto createNote(@RequestBody NoteStageDto noteStageDto,
                                   @PathVariable("idStage") Integer idStage){
        try {
            noteStageDto = noteStageService.createNoteStage(noteStageDto, idStage);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la création d'une note",e
            );
        }

        return noteStageDto;
    }

    /**
     * Récupère la liste des notes selon le stage
     * @param idStage
     * @return
     */
    @GetMapping("/stage/note/{idStage}")
    public List<NoteStageDto> getListNoteStageByIdStage(@PathVariable("idStage") Integer idStage){
        return noteStageService.getListNoteStageByIdStage(idStage);
    }

    /**
     * Récupère une note selon son id
     * @param idNoteStage
     * @return
     */
    @GetMapping("/note/{idNoteStage}")
    public NoteStageDto getNoteStageById(@PathVariable("idNoteStage") Integer idNoteStage){
        return noteStageService.getNoteStageById(idNoteStage);
    }

    /**
     * Modification de la note d'un stage
     * @param noteStageDto
     * @return
     */
    @PutMapping("/update")
    public NoteStageDto updateNoteStage(@RequestBody NoteStageDto noteStageDto){
        try {
            noteStageDto = noteStageService.updateNoteStage(noteStageDto);
        } catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification de la note",e
            );
        }
        return noteStageDto;
    }

    /**
     * Supprime une note d'un stage
     * @param idNoteStage
     */
    @DeleteMapping("/delete/{idNoteStage}")
    public void deleteNoteStage(@PathVariable("idNoteStage") Integer idNoteStage){
        try {
            noteStageService.deleteNoteStage(idNoteStage);
        } catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la suppression de la note",e
            );
        }
    }
}
