package org.ressourcesarea.MSStage.application.error;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final ObjectMapper mapper = new ObjectMapper();

    @ExceptionHandler(value = {GeneralException.class})
    protected ResponseEntity<Object> handleConflict (RuntimeException exception, WebRequest request){
        ObjectNode bodyObject = mapper.createObjectNode();
        bodyObject.put("error", exception.getMessage());
        return handleExceptionInternal(exception, bodyObject, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {EntityException.class})
    protected ResponseEntity<Object> handleEntityException ( RuntimeException exception, WebRequest request){
        ObjectNode bodyObject = mapper.createObjectNode();
        bodyObject.put("error", exception.getMessage());
        return handleExceptionInternal(exception, bodyObject, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = { BadCredentialsException.class})
    protected ResponseEntity<Object> handleAuthException ( RuntimeException exception, WebRequest request){
        ObjectNode bodyObject = mapper.createObjectNode();
        bodyObject.put("error", "Le matricule ou le mot de passe n'existe pas");
        return handleExceptionInternal(exception, bodyObject, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = { AuthorizeException.class})
    protected ResponseEntity<Object> handleAuthorizeException ( RuntimeException exception, WebRequest request){
        ObjectNode bodyObject = mapper.createObjectNode();
        bodyObject.put("error", exception.getMessage());
        return handleExceptionInternal(exception, bodyObject, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }
    // NOT Authorize ...

    // Revoir les différentes Exception possible ( détailler plus les exceptions )
    // .... Différentes Exception à gérer côté métier
}
