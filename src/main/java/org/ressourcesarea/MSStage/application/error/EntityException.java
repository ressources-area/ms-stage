package org.ressourcesarea.MSStage.application.error;

public class EntityException extends RuntimeException {
    public EntityException(String message){
        super(message);
    }
}
