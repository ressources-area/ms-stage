package org.ressourcesarea.MSStage.application.error;

public class GeneralException extends RuntimeException {
    public GeneralException(String message){
        super(message);
    }
}
