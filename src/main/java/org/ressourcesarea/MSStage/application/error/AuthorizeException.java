package org.ressourcesarea.MSStage.application.error;

public class AuthorizeException extends RuntimeException {
    public AuthorizeException(String message){
        super(message);
    }
}
