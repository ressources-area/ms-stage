package org.ressourcesarea.MSStage.security;

import org.ressourcesarea.MSStage.utils.AgentProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class JwtRequestFilter extends OncePerRequestFilter {

    private final AgentProxy agentProxy;

    @Autowired
    public JwtRequestFilter(AgentProxy agentProxy){
        this.agentProxy = agentProxy;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws ServletException, IOException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        res.setHeader("Access-Control-Allow-Origin",request.getHeader("origin"));
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        res.setHeader("Access-Control-Max-Age", "3600");
        res.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");

        try {
            String jwt = getJwtFromCookie(request);
            if (agentProxy.authSuccess(jwt) || "OPTIONS".equals(request.getMethod())){
                filterChain.doFilter(request,response);
            } else {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

    }

    private String getJwtFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if(cookies==null)
            return "";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("AuthToken")) {
                String accessToken = cookie.getValue();
                if (accessToken == null) {
                    return null;
                }
                return accessToken;
            }
        }
        return null;
    }
}
