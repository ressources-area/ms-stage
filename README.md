# Ressources-Area - MS-Stage
## Description
L'api MS-Stage est un service de gestion des stages de formation.
Lien : https://gitlab.com/ressources-area/ms-stage
## Développement 
Le service est développé avec Intellij et et java JDK 11.
Il utilise SpringBoot 2.4.2.

## Installation et Run 
1) L'api nécessite une une base de données PostgreSQL.
Pour utiliser la base de données utile au service.
   Installer Docker qui permettra de lancer la base de données
   avec les créations de tables.
   
2) Packager le service à l'aide de la commande "mvn package" dans le dossier "MS-Stage",
ensuite entrer la commande "java -jar MS-Stage-0.0.1.jar".

3) Lancer le service "ms-agent", il est utile au fonctionnement de ce service.
   Lien : https://gitlab.com/ressources-area/ms-agent
   
Le service est lancé.

## Owner
BRICHET Benoît - brichet.b53@gmail.com

Ressources-Area a été développé dans le cadre d'un projet final
d'une formation de développeur Java via OpenClassrooms
